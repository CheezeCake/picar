CXX=clang++
CXXFLAGS=-std=c++14 -pipe -O2
#CXXFLAGS=-std=c++14 -pipe -g -ggdb
WPI=-lwiringPi -lpthread

all: piCar

piCar: main.o server.o car.o
	$(CXX) $^ -o $@ $(WPI)

main.o: main.cpp server.hpp car.hpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

server.o: server.cpp server.hpp car.hpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

car.o: car.cpp car.hpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	@rm -f *.o

mrproper: clean
	@rm -f piCar
