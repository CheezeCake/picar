#include <wiringPi.h>
#include <iostream>
#include <softPwm.h>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <csignal>
#include <cmath>
#include <cassert>
#include "car.hpp"

volatile unsigned int Car::telemeterEchoTimeStart = 0;
volatile unsigned int Car::telemeterLastEchoTime = 0;
bool Car::obstacleBack = false;
bool Car::waitingForEcho = false;
bool Car::obstacleFirstTime = false;
volatile float Car::leftSpeed = 0;
volatile float Car::rightSpeed = 0;

Car *Car::instance = nullptr;

Car *Car::getInstance()
{
	if(!instance)
		instance = new Car;
	return instance;
}

Car::Car()
{
	if(wiringPiSetup() == -1)
		exit(1);

	pinMode(pwm_arg, PWM_OUTPUT);
	pinMode(pwm_ard, PWM_OUTPUT);
	pinMode(pwm_avg, PWM_OUTPUT);
	pinMode(pwm_avd, PWM_OUTPUT);

	pinMode(forw_arg, OUTPUT);
	pinMode(back_arg, OUTPUT);

	pinMode(forw_ard, OUTPUT);
	pinMode(back_ard, OUTPUT);

	pinMode(forw_avg, OUTPUT);
	pinMode(back_avg, OUTPUT);

	pinMode(forw_avd, OUTPUT);
	pinMode(back_avd, OUTPUT);

	pinMode(left_encoder, INPUT);
	pinMode(right_encoder, INPUT);

	pinMode(telemeter_trigger, OUTPUT);
	pinMode(telemeter_echo, INPUT);

	pinMode(horizontal_servo, OUTPUT);
	pinMode(vertical_servo, OUTPUT);

	softPwmCreate(pwm_arg, 0, 100); // returns 0 in case of success
	softPwmCreate(pwm_ard, 0, 100);
	softPwmCreate(pwm_avg, 0, 100);
	softPwmCreate(pwm_avd, 0, 100);

	init();

	wiringPiISR(left_encoder, INT_EDGE_RISING, &Car::leftEncoderISR);
	wiringPiISR(right_encoder, INT_EDGE_RISING, &Car::rightEncoderISR);
	wiringPiISR(telemeter_echo, INT_EDGE_BOTH, &Car::telemeterISR);

	instance = this;
}

void Car::triggerTelemeter()
{
	if((telemeterLastEchoTime - millis() >= 60) || telemeterLastEchoTime == 0)
	{
		digitalWrite(telemeter_trigger, HIGH);
		telemeterEchoTimeStart = millis();
		delayMicroseconds(10);
		digitalWrite(telemeter_trigger, LOW);
		telemeterLastEchoTime = millis();
		Car::waitingForEcho = true;
	}
}

void Car::leftGo(bool forward, int speed)
{
	if(Car::obstacleBack && !forward)
		return;

	if(forward)
	{
		Car::obstacleBack = false;
		Car::obstacleFirstTime = false;
		digitalWrite(forw_avg, HIGH);
		digitalWrite(forw_arg, HIGH);
		digitalWrite(back_avg, LOW);
		digitalWrite(back_arg, LOW);
	}
	else
	{
		digitalWrite(back_avg, HIGH);
		digitalWrite(back_arg, HIGH);
		digitalWrite(forw_avg, LOW);
		digitalWrite(forw_arg, LOW);
	}
	softPwmWrite(pwm_avg, speed);
	softPwmWrite(pwm_arg, speed);
}

void Car::rightGo(bool forward, int speed)
{
	if(Car::obstacleBack && !forward)
		return;

	if(forward)
	{
		Car::obstacleBack = false;
		Car::obstacleFirstTime = false;
		digitalWrite(forw_avd, HIGH);
		digitalWrite(forw_ard, HIGH);
		digitalWrite(back_avd, LOW);
		digitalWrite(back_ard, LOW);
	}
	else
	{
		digitalWrite(back_avd, HIGH);
		digitalWrite(back_ard, HIGH);
		digitalWrite(forw_avd, LOW);
		digitalWrite(forw_ard, LOW);
	}
	softPwmWrite(pwm_avd, speed);
	softPwmWrite(pwm_ard, speed);
}

void Car::init()
{
	digitalWrite(forw_arg, LOW);
	digitalWrite(back_arg, LOW);
	digitalWrite(forw_ard, LOW);
	digitalWrite(back_ard, LOW);
	digitalWrite(forw_avg, LOW);
	digitalWrite(back_avg, LOW);
	digitalWrite(forw_avd, LOW);
	digitalWrite(back_avd, LOW);
	softPwmWrite(pwm_avg, 0);
	softPwmWrite(pwm_arg, 0);
	softPwmWrite(pwm_avd, 0);
	softPwmWrite(pwm_ard, 0);
}

float Car::getSpeed()
{
	return ((leftSpeed + rightSpeed) / 2);
}

void Car::telemeterISR()
{
	if(!Car::waitingForEcho)
		return;

	int status = digitalRead(telemeter_echo);
	if(status == LOW)
	{
		unsigned int temp = millis() - telemeterEchoTimeStart;
		unsigned int distance = temp * 1000 / 58;
		telemeterEchoTimeStart = 0;
		//printf("Echo end (%d millis)\n", temp);
		printf("Distance : %d cm\n", distance);
		if(distance <= OBSTACLE_LIMIT && distance != 0)
		{
			if(!Car::obstacleFirstTime)
				Car::obstacleFirstTime = true;

			else
			{
				printf("STOP\n");
				instance->init();
				Car::obstacleBack = true;
				Car::obstacleFirstTime = false;
			}
		}
		else
			Car::obstacleFirstTime = false;

		Car::waitingForEcho = false;
	}
	/*else
	{
		telemeterEchoTimeStart = millis();
		printf("Echo start\n");
	}*/
}

void Car::leftEncoderISR()
{
	static volatile unsigned int oldTime = 0;
	static volatile unsigned int nbCalls = 0;

	//std::cout << __func__ << '\n';
	++nbCalls;

	unsigned int diff = millis() - oldTime;

	if (diff >= 1000) {
		leftSpeed = computeSpeed(diff, nbCalls);

		nbCalls = 0;
		oldTime = millis();
	}
}

void Car::rightEncoderISR()
{
	static volatile unsigned int oldTime = 0;
	static volatile unsigned int nbCalls = 0;

	//std::cout << __func__ << '\n';
	++nbCalls;

	unsigned int diff = millis() - oldTime;

	if (diff >= 1000) {
		rightSpeed = computeSpeed(diff, nbCalls);

		nbCalls = 0;
		oldTime = millis();
	}
}

inline float Car::computeSpeed(unsigned int millis, unsigned int nbPulses)
{
	constexpr float wheelDiameterInMeters = 0.065; // 6.5cm

	const float diffInSec = (float)millis / 1000;
	const float rps = ((float)nbPulses / 20.0) / diffInSec;

	return (rps * wheelDiameterInMeters * M_PI);
}

void Car::initServo(int servoPin, int position, unsigned int timeMillis)
{
	const unsigned int start = millis();
	while (millis() - start < timeMillis)
	{
		digitalWrite(servoPin, HIGH);
		delayMicroseconds(position);
		digitalWrite(servoPin, LOW);
		delayMicroseconds(servo_resolution_us - position);
	}
}

void Car::servoControlWorker(int pipe, int servoPin, int initialPosition,
		int minLimit, int maxLimit)
{
	assert(minLimit < maxLimit);
	assert(initialPosition >= minLimit);
	assert(initialPosition <= maxLimit);

	initServo(servoPin, initialPosition, 500);

	int servoValue = initialPosition;
	int8_t joystickValue;
	uint64_t lastPulseStart = 0;

	while (1)
	{
		if (read(pipe, &joystickValue, 1) > 0)
		{
			joystickValue = -joystickValue;
			const int newServoValue = servoValue + joystickValue;

			if (newServoValue > maxLimit)
			{
				if (servoValue == maxLimit)
					continue;
				servoValue = maxLimit;
			}
			else if (newServoValue < minLimit)
			{
				if (servoValue == minLimit)
					continue;
				servoValue = minLimit;
			}
			else
			{
				servoValue = newServoValue;
			}

			const uint64_t delta = micros() - lastPulseStart;
			if (delta < servo_resolution_ms)
				delayMicroseconds(servo_resolution_us - delta);

			lastPulseStart = micros();

			digitalWrite(servoPin, HIGH);
			delayMicroseconds(servoValue);
			digitalWrite(servoPin, LOW);
		}
		else
		{
			perror("servo worker pipe");
		}
	}
}
