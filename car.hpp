#pragma once

#define pwm_arg 0
#define forw_arg 2
#define back_arg 3

#define pwm_ard 12
#define forw_ard 14
#define back_ard 13

#define pwm_avg 1
#define forw_avg 5
#define back_avg 4

#define pwm_avd 6
#define forw_avd 11
#define back_avd 10

#define left_encoder 15
#define right_encoder 16

#define telemeter_trigger 18
#define telemeter_echo 20

#define horizontal_servo 7
#define vertical_servo 17

#define servo_min_position_limit 600
#define servo_neutral_position 1500
#define servo_max_position_limit 2400
#define servo_resolution_us 20'000
#define servo_resolution_ms 20

#define vertical_servo_initial_position 2310
#define vertical_servo_min_position 1800

#define horizontal_servo_min_position 900
#define horizontal_servo_max_position 2000

#define OBSTACLE_LIMIT 20

class Car
{
	public:
		static Car *getInstance();
		Car();
		void leftGo(bool forward, int speed);
		void rightGo(bool forward, int speed);
		void init();
		float getSpeed();
		void triggerTelemeter();

		void servoControlWorker(int pipe, int servoPin, int initialPosition,
				int minLimit, int maxLimit);

	private:
		static void telemeterISR();
		static void leftEncoderISR();
		static void rightEncoderISR();
		static float computeSpeed(unsigned int millis, unsigned int nbPulses);
		void initServo(int servoPin, int position, unsigned int timeMillis);

		static Car *instance;

		static volatile float leftSpeed;
		static volatile float rightSpeed;

		static volatile unsigned int telemeterEchoTimeStart;
		static volatile unsigned int telemeterLastEchoTime;

		static bool obstacleBack;
		static bool waitingForEcho;
		static bool obstacleFirstTime;
};
