#include <iostream>
#include <csignal>
#include <cstdlib>
#include "server.hpp"

Server server;

void sig(int)
{
	std::cout << "Reseting pins" << std::endl;
	server.initCar();
	std::cout << "Server cleanup" << '\n';
	exit(0);
}

int main(int argc, char** argv)
{
	signal(SIGINT, sig);

	return server.run();
}
