#include <iostream>
#include <system_error>
#include <thread>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdio>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <unistd.h>
#include <wiringPi.h>
#include "server.hpp"

Server::Server()
{
	car = *(Car::getInstance());
	car.init();

	socketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	if(socketFD == -1)
		throw std::system_error(errno, std::system_category());

	const int opt = 1;
	if (setsockopt(socketFD, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) == -1)
	{
		close(socketFD);
		throw std::system_error(errno, std::system_category());
	}

	sockaddr_in sa;
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = PF_INET;
	sa.sin_port = htons(1100);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(socketFD, reinterpret_cast<sockaddr*>(&sa), sizeof(sa)) ==  -1)
	{
		close(socketFD);
		throw std::system_error(errno, std::system_category());
	}

	if(listen(socketFD, 10) == -1)
	{
		close(socketFD);
		throw std::system_error(errno, std::system_category());
	}

	int horizontalPipe[2];
	if (pipe(horizontalPipe) == -1)
	{
		close(socketFD);
		throw std::system_error(errno, std::system_category());
	}

	int verticalPipe[2];
	if (pipe(verticalPipe) == -1)
	{
		close(horizontalPipe[0]);
		close(horizontalPipe[1]);
		close(socketFD);
		throw std::system_error(errno, std::system_category());
	}

	horizontalServoWorkerPipe = horizontalPipe[1];
	std::thread(&Car::servoControlWorker, std::ref(car), horizontalPipe[0],
			horizontal_servo, servo_neutral_position, horizontal_servo_min_position,
			horizontal_servo_max_position).detach();

	verticalServoWorkerPipe = verticalPipe[1];
	std::thread(&Car::servoControlWorker, std::ref(car), verticalPipe[0],
			vertical_servo, vertical_servo_initial_position, vertical_servo_min_position,
			servo_max_position_limit).detach();
}

Server::~Server()
{
	close(socketFD);
}

void Server::cleanup()
{
	close(socketFD);

	close(horizontalServoWorkerPipe);
	close(verticalServoWorkerPipe);
}

void Server::initCar()
{
	car.init();
}

int Server::run()
{
	while (1)
	{
		std::cout << "waiting on accept\n";
		int connectFD = accept(socketFD, nullptr, nullptr);

		if(connectFD < 0)
		{
			perror("accept failed");
		}
		else
		{
			std::cout << "connect\n";
			handleClient(connectFD);
		}
	}

	return EXIT_SUCCESS;  
}

void Server::moveCar(int x, int y)
{
	bool forward = (y <= 0);
	bool right = (x >= 0);

	//std::cout << "x = " << x << ", y = " << y << '\n';

	y = abs(y);
	x = abs(x);

	if (x == 0 && y == 0)
	{
		car.leftGo(forward, 0);
		car.rightGo(forward, 0);
	}
	else if (x <= 100 && y <= 100)
	{
		if (right)
		{
			car.leftGo(forward, std::max(x, y));
			car.rightGo(forward, std::max(y - x, 0));
		}
		else
		{
			car.leftGo(forward, std::max(y - x, 0));
			car.rightGo(forward, std::max(x, y));
		}
	}
	if(!forward)
		car.triggerTelemeter();
}

void Server::moveCamera(int8_t x, int8_t y)
{
	//send command to workerThreads
	write(horizontalServoWorkerPipe, &x, 1);
	write(verticalServoWorkerPipe, &y, 1);
}

void Server::sendSpeed(int socket)
{
	float speed = car.getSpeed();
	//std::cout << "speed = " << speed << '\n';

	uint32_t speedInt = *reinterpret_cast<uint32_t*>(&speed);
	speedInt = htonl(speedInt);
	send(socket, &speedInt, 4, MSG_NOSIGNAL);
}

void Server::handleClient(int connectFD)
{
	uint8_t buffer[MAX_CMD_SIZE] = {0};
	size_t offset = 0;

	while (1)
	{
		ssize_t bytesRead = recv(connectFD, &buffer[offset], sizeof(buffer) - offset, 0);
		offset += bytesRead;

		if (bytesRead == -1 || bytesRead == 0)
		{
			std::cout << "bytesRead = " << bytesRead << '\n';

			if(shutdown(connectFD, SHUT_RDWR) == -1)
				perror("shutdown failed");

			close(connectFD);

			return;
		}
		else if (offset == SPEED_CMD_SIZE)
		{
			//std::cout << "offset = " << offset << ", prefix = " << buffer[0] << '\n';
			if (buffer[0] == SPEED_PREFIX) {
				sendSpeed(connectFD);

				memset(buffer, 0, sizeof(buffer));
				offset = 0;
			}
		}
		else if (offset == MOVE_CMD_SIZE)
		{
			//std::cout << "offset = " << offset << ", prefix = " << buffer[0] << '\n';
			if (buffer[0] == MOTORS_PREFIX || buffer[0] == CAMERA_PREFIX)
			{
				int8_t x = buffer[1];
				int8_t y = buffer[2];

				if (buffer[0] == MOTORS_PREFIX)
					moveCar(x, y);
				else
					moveCamera(x, y);

				memset(buffer, 0, sizeof(buffer));
				offset = 0;
			}
		}
	}
}
