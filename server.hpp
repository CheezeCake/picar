#pragma once

#include <limits>
#include "car.hpp"

class Server
{
	static_assert(std::numeric_limits<float>::is_iec559, "Requires IEEE 754 floating point");

	private:
		Car car;

		int socketFD;
		int horizontalServoWorkerPipe;
		int verticalServoWorkerPipe;

		void handleClient(int sock);
		void moveCar(int x, int y);
		void moveCamera(int8_t x, int8_t y);
		void sendSpeed(int socket);

		static constexpr ssize_t MOVE_CMD_SIZE = 3;
		static constexpr ssize_t SPEED_CMD_SIZE = 1;

		static constexpr ssize_t MAX_CMD_SIZE = MOVE_CMD_SIZE;

		static const uint8_t SPEED_PREFIX = 's';
		static const uint8_t MOTORS_PREFIX = 'm';
		static const uint8_t CAMERA_PREFIX = 'c';

	public:
		Server();
		~Server();
		void initCar();
		int run();
		void cleanup();
};
